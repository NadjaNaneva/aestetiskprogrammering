# Describe your program and what you have used and learnt.

I min miniX2 har jeg valgt at lave nogle emojis, hvor hovedpointen er at der skal være et skelet med nogle af de velkendte 'expression' vi kender fra de emojis der er i dag. Jeg har lavet tre forskellige. En med hjerter som øjne, en med stjerner og til sidst en emoji der græder.

#### EMOJI 1 
Dette er emojien der har hjerter som øjne. Hjerterne bliver større; det kan du se ved at klikke på linket. Der er også et screenshot af emojien som også kan ses nedenfor.

[klik her for at se hjerte emoji](https://amaliedoktor.gitlab.io/aestetiskprogrammering/MiniX2/index.html)

[source code](https://gitlab.com/AmalieDoktor/aestetiskprogrammering/-/blob/main/MiniX2/emoji.js)

![](https://amaliedoktor.gitlab.io/aestetiskprogrammering/MiniX2/skr%C3%A6mbillede_hjerter.png)

#### EMOJI 2 
Emojien med stjerner som øjne. Den skal give illusionen af en imponeret emoji, hvor stjernerne også har samme effekt som hjerterne.

[klik her for at se stjerne emoji](http://amaliedoktor.gitlab.io/aestetiskprogrammering/MiniX2/index2.html)

[source code](https://gitlab.com/AmalieDoktor/aestetiskprogrammering/-/blob/main/MiniX2/emoji2.js)

![](https://amaliedoktor.gitlab.io/aestetiskprogrammering/MiniX2/sk%C3%A6rmbillede_stjerner.png)

#### EMOJI 3 
En emoji der er trist og har tårer løbende ned af kinderne.

[klik her for at se trist emoji](https://amaliedoktor.gitlab.io/aestetiskprogrammering/MiniX2/index3.html)

[source code](https://gitlab.com/AmalieDoktor/aestetiskprogrammering/-/blob/main/MiniX2/emoji3.js)

![](https://amaliedoktor.gitlab.io/aestetiskprogrammering/MiniX2/sk%C3%A6rmbillede.png) 

I forhold til kodning, så begyndte jeg med at indsætte billedet af kraniet. Jeg havde ikke prøvet at indsætte et billede før, men ved hjælp af referencelisten lykkedes det mig forholdsvis hurtigt at indsætte billedet via funktionen `preload()`. Den fulde variabel/funktion kan ses nedenfor:

```
let img;
let img2;
let stoerrelse;
function preload() {
  img = loadImage('../MiniX2/dodningehoved.png');
  img2 = loadImage("../MiniX2/hjerte.png")
}
```
Jeg kan forklare processen omkring emoji 1, da det var den jeg startede med. Jeg indsatte billederne af hjerterne også via `preload()`, og flyttede dem på x- og y-aksen så de var placeret foran øjnene. Det gjorde jeg ved at bruge funktionen `image()` hvor jeg indsatte værdier for placeringen og størrelsen. Da det måske blev lidt for kedeligt at der bare var et kranie med hjerteøjne, så begyndte jeg at tænke på at få hjerterne til at bevæge sig. 

Jeg prøvede at gøre nogle forskellige ting med variabler, men synes ikke jeg kunne få det til at lykkedes uden at hjerterne blev helt deforme. Men i sidste ende lykkedes det med denne variabel:

```
  stoerrelse = random(150, 190);
  frameRate(10);
  image(img, 0, 0, 570, 500);
  push()
  imageMode(CENTER);
  image(img2, 220, 240, stoerrelse, stoerrelse);
  image(img2, 365, 240, stoerrelse, stoerrelse);
  pop()

  ```
Jeg lavede en ny variable med navnet `stoerrelse` og brugte den i stedet for talværdierne i `image()` funktionerne. På den måde undgik jeg at hjertene blev 'strukket' forkert. Jeg benyttede mig af funktionen `random()` som får hjerterne til at blive tilfældige størrelser indefor de to parametere 150, 190. Derfor endte jeg med at få effekten af at hjerterne vokser og går tilbage til normalen igen.

I starten voksede hjerterne meget hurtigt, så jeg valgte at sætte `frameRate()` ned til 10 som gav emojien et bedre udseende.

Jeg gentog hele processen for at skabe emoji 2, men ved emoji 3 lavede jeg to `rect()` som jeg derefter fyldte ud med en blå farve via `fill()` for at få det til at ligne tårer. De blev placeret under øjnene og skabte den trist emoji der kan ses på den tredje screenshot. Jeg havde egentlig tænkt mig at få tårerne til at 'løbe' ned af kinderne, men desværre havde jeg ikke nok tid til at sætte mig ind i det denne gang. Jeg ville nok have brugt en ny variable og ved at indsætte `let rektangel + = 10` eller ligende på y-aksen så få tårerne til at trille.



# How would you put your emoji into a wider social and cultural context that concerns a politics of representation, identity, race, colonialism, and so on?

Jeg brugte lang tid på at brainstorme om hvordan man kan lave en emoji der ikke er discriminerede eller kun repræsenterer nogle specifikke typer af mennesker.
Jeg valgte i sidste ende at arbejde ud fra en sætning fra en af de tekster vi har læst.

_"With the comment, we attempted to argue that it does not make 
sense to fix these issues by finding a less controversial standard 
for expressing skin tone, or to solve the problem by adding yet 
more variables, as the mechanism of varying between binary 
oppositions itself is fundamentally flawed."_ (Abbing, Pierrot, & Snelting, s. 46)

Jeg synes det her eksempel havde et interessant syn, nemlig at tilføje mere ikke nødvendigvis hjælper på problematikken med manglende repræsentation af bestemte mennesker. Derfor, så valgte jeg at tage en hel ny vinkel.

Når man tænker over det, så er der meget ved os mennesker der er forskelligt og hvis man skal lave emojis for hver enkel ting, så vil det forudsage uendeligt mange emojis. Derfor tænkte jeg over hvilken del af mennesker der er stort set ens uanset hvem man er. Det gjorde at jeg faldte jeg over tanken om skeletter.

Uanset hvilken hudfarve du har, om du er mand eller kvinde, ung eller gammel, så har du et kranie der ser nogenlunde ens ud med alle andres. Derfor fik jeg ideen om at lave mine emojis ved hjælp af kranier, da de ikke udelukker nogle bestemte mennesker - alle mennesker har et kranie i en eller anden facon. (man kan så diskutere om kranier kan se forskellige ud, men siden vi ikke kan se vores egne kranier, kan man forestille sig at det vil skabe markant mindre debat en f.eks. udfarve og køn). Der er dog stadig en problematik i de emojis der ikke har med en 'menneskelig' skikkelse at gøre, som f.eks. mad, biler og lignende, men det var ikke mit fokus i denne MiniX opgave.

Jeg valgte at holde mit fokus på den manglende repræsentation af bestemte grupper af mennesker. Derfor har jeg lavet nogle emojis der kan repræsentere os alle sammen.



## Referenceliste:

Roel Roscam Abbing, Peggy Pierrot and Femke Snelting, _“Modifying the Universal,”_ in Helen Pritchard, Eric Snodgrass & Magda Tyżlik-Carver, eds., _Executing Practices_ (London: Open Humanities Press, 2018), 35-51

