function setup() {
  // put setup code here
  createCanvas(1200,1200);
  background(0,200,255);
}



function draw() {
  // put drawing code here
  
  if(frameCount %6 == 0) 
  {
    push()
  frameRate(5)
  fill(0,200,255);
  noStroke()
}
  //N
  noStroke()
  square(200,200,20);
  square(200,210,20);
  square(200,220,20);
  square(200,230,20);
  square(200,240,20);
  square(200,250,20);
  square(200,260,20);
  square(200,270,20);
  square(200,280,20);
  square(210,215,20); // begynder på sidelæns
  square(220,230,20);
  square(230,245,20);
  square(240,260,20);
  square(250,275,20);
  square(260,280,20);
  square(260,270,20);//begynder at gå op igen
  square(260,260,20);
  square(260,250,20);
  square(260,240,20);
  square(260,230,20);
  square(260,220,20);
  square(260,210,20);
  square(260,200,20);
  //begynder på E
  square(290,280,20);
  square(290,270,20);
  square(290,260,20);
  square(290,250,20);
  square(290,240,20);
  square(290,230,20);
  square(290,220,20);
  square(290,210,20);
  square(290,200,20);
  //begynder på nederste streg
  square(300,280,20);
  square(310,280,20);
  square(320,280,20);
  square(330,280,20);
  //begynder på midterstreg
  square(300,240,20);
  square(310,240,20);
  //begynder på øverste streg
  square(300,200,20);
  square(310,200,20);
  square(320,200,20);
  square(330,200,20);
  //begynder på w
  square(360,200,20);
  square(360,210,20);
  square(360,220,20);
  square(360,230,20);
  square(360,240,20);
  square(360,250,20);
  square(370,260,20);
  square(380,270,20);
  square(400,280,20);//begynder på midten
  square(400,270,20);
  square(400,260,20);
  square(400,250,20);
  square(400,240,20);
  square(400,230,20);
  square(400,220,20);
  square(400,210,20);
  square(400,200,20);
  square(420,270,20);//begynder på højre side
  square(430,260,20);
  square(440,250,20);
  square(440,240,20);
  square(440,230,20);
  square(440,220,20);
  square(440,210,20);
  square(440,200,20);
  //begynder på G
  square(500,220,20);
  square(500,230,20);
  square(500,240,20);
  square(500,250,20);
  square(505,260,20);
  square(510,270,20);
  square(515,280,20);
  square(520,280,20);
  square(530,280,20);
  square(540,280,20);
  square(550,270,20);
  square(555,260,20);
  square(555,250,20);
  square(555,240,20);
  square(550,240,20);
  square(540,240,20);
  square(505,210,20);
  square(510,200,20);
  square(520,200,20);
  square(530,200,20);
  square(540,200,20);
  square(550,205,20);
  square(555,210,20);
  //begynder på A
  square(585,230,20);
  square(585,240,20);
  square(585,250,20);
  square(585,260,20);
  square(585,280,20);
  square(585,270,20);
  square(595,215,20);
  square(605,200,20);
  square(615,200,20);
  square(625,215,20);
  square(635,225,20);
  square(635,230,20);
  square(635,240,20);
  square(635,250,20);
  square(635,260,20);
  square(635,270,20);
  square(635,280,20);
  //midterstreg
  square(620,250,20);
  square(610,250,20);
  square(600,250,20);
  //begynder på M
  square(665,280,20);
  square(665,270,20);
  square(665,260,20);
  square(665,250,20);
  square(665,240,20);
  square(665,230,20);
  square(665,220,20);
  square(665,210,20);
  square(665,200,20);
  square(670,210,20);
  square(680,220,20);
  square(695,230,20);
  square(710,220,20);
  square(720,210,20);
  square(730,200,20);
  square(730,210,20);
  square(730,220,20);
  square(730,230,20);
  square(730,240,20);
  square(730,250,20);
  square(730,260,20);
  square(730,270,20);
  square(730,280,20);
  //begynder på E
  square(760,280,20);
  square(760,270,20);
  square(760,260,20);
  square(760,250,20);
  square(760,240,20);
  square(760,230,20);
  square(760,220,20);
  square(760,210,20);
  square(760,200,20);
  //begynder på nederste linje
  square(770,280,20);
  square(780,280,20);
  square(790,280,20);
  square(800,280,20);
  //begynder på øverste linje
  square(770,200,20);
  square(780,200,20);
  square(790,200,20);
  square(800,200,20);
  //begynder på midterste linje
  square(770,240,20);
  square(780,240,20);
  fill(50,0,255)
  pop()

  push()
  //begynder på nyt ord
  fill(0,250,0);
  textFont('courier New');
  textSize(50);
  text('options', 402, 400);
  textSize(50);
  text('multiplayer', 358, 470);
  textSize(50);
  text('levels', 422, 540);
  textSize(50);
  text('exit', 442, 610);
  pop()

  push()
  fill(0,0,255);
  textFont('courier New');
  textSize(50);
  text('options', 405, 400);
  textSize(50);
  text('multiplayer', 360, 470);
  textSize(50);
  text('levels', 425, 540);
  textSize(50);
  text('exit', 445, 610);
  pop()

  push()
  fill(250);
  //ny sky
  square(520,70,50,70,70,70,70);
  square(480,90,60,70,70,0,70);
  square(460,110,50,70,70,0,70);
  square(540,100,60,70,70,70,0);
  square(560,70,60,40,40,40,40);
  square(570,100,60,0,70,70,0);
  square(510,100,60,0,70,70,0);
  //ny sky
  square(210,400,50,30,30,30,30);
  square(170,420,60,30,30,0,30);
  square(150,440,50,30,30,0,30);
  square(230,430,60,30,30,30,0);
  square(250,400,60,40,40,40,40);
  square(260,430,60,0,30,30,0);
  square(200,430,60,0,20,20,0);
  //ny sky
  square(810,500,50,30,30,30,30);
  square(770,520,60,30,30,0,30);
  square(750,540,50,30,30,0,30);
  square(830,530,60,30,30,30,0);
  square(850,500,60,40,40,40,40);
  square(860,530,60,0,30,30,0);
  square(800,530,60,0,20,20,0);

  pop()
  

}





