# MiniX 1
## Idefasen
Jeg har i min MiniX 1 valgt at tage inspiration fra videospil/arcade spil. Min ide var at lave en "game menu" ved at have en tekst, der siger NEW GAME imens det blinker og nedenunder have nogle andre ord, for at skabe et udseende der er retro.

Samtidig vil jeg gerne gøre lidt ud af baggrunden, hvor jeg valgte at lave den blå og derefter kode nogle skyer for at give illusionen af, at menuen er oppe i luften. 

Herunder kan I se det færdige produkt, hvor jeg efterfølgende vil komme ind på udførelsen af idefasen.

[Klik her for at se MiniX1](https://amaliedoktor.gitlab.io/aestetiskprogrammering/MiniX1/index.html)

[Klik her for at se source code](https://gitlab.com/AmalieDoktor/aestetiskprogrammering/-/blob/main/MiniX1/gamestart.js)

Screenshot af MiniX 1 kan også ses her:

![](https://amaliedoktor.gitlab.io/aestetiskprogrammering//MiniX1/Sk%C3%A6rmbillede_2023-02-11_153810.png)

## Udførelsen

#### Canvas
Som det første startede jeg selvfølgelig med at lave et Canvas. Jeg lavede det 1200x1200 i størrelsen, hvorefter jeg gav det en farve ved at bruge funktionen `background(0,200,250)`, som giver det den blå farve der kan ses på billedet.

#### Teksten 

Derefter kastede jeg mig ud i at skrive NEW GAME på en måde der nu virker meget upraktisk og tidskrævende. Jeg valgte at lave ordet gennem firkanter, altså at sætte en masse firkanter sammen for at danne et ord. Dette var før jeg vidste at der findes en funktion der kan lave tekst: `text()`. Da jeg lavede NEW GAME med firkanterne tænkte jeg, at kodning virkelig var unødvendigt besværligt. Men heldigvis har jeg lært noget fra alt det ekstra arbejde jeg endte med at udføre alligevel. Siden jeg skulle flytte hver lille firkant, så blev jeg meget bekendt med dimensionerne af X og Y-aksen. Dog valgte jeg at bruge `text()` funktionen til at skrive de tre ord under NEW GAME, hvilket gik super hurtigt og var forholdsvis nemt at gøre.

Herunder ser du koden fra en af bogstaverne, hvor man kan se koden for hver af de små firkanter som danner bogstavet.

```
  square(200,200,20);
  square(200,210,20);
  square(200,220,20);
  square(200,230,20);
  square(200,240,20);
  square(200,250,20);
  square(200,260,20);
  square(200,270,20);
  square(200,280,20);
  square(210,215,20); // begynder på sidelæns
  square(220,230,20);
  square(230,245,20);
  square(240,260,20);
  square(250,275,20);
  square(260,280,20);
  square(260,270,20); //begynder at gå op igen
  square(260,260,20);
  square(260,250,20);
  square(260,240,20);
  square(260,230,20);
  square(260,220,20);
  square(260,210,20);
  square(260,200,20);
```
Jeg skulle efterfølgende farve de her firkanter, hvor jeg så vil benytte funktionen `fill()`. Men dette var noget jeg valgte at gøre efter jeg havde lavet alt tekst på mit canvas. Men jeg ville gerne undgå, at alt teksten blev den samme farve. Jeg havde svært ved at finde ud af, hvordan jeg kunne dele de forskellige tekster fra hinanden. Så jeg blev nødt til at spørge mig omkring og blev så bekendt med nye syntakser inde fra referencelisten, nemlig `push()` og `pop()`. Hvis man sætter disse to funktioner rundt om f.eks. en masse linjer kode til en af teksterne, så skaber de ligesom en "ramme" rundt om koderne og gør, at kun funktionerne indenfor push/pop kan påvirke hinanden.

Derefter lykkedes det mig at give teksterne forskellige farver og sørge for at skyerne blev hvide frem for blå.

Nu var alt den tekst og diverse detaljer jeg gerne vil have med blevet kodet. Derefter vil jeg så kaste mig i af få NEW GAME til at blinke, hvilket skulle vise sig at blive et længere projekt. Jeg søgte hjælp online, hvor jeg opdagede at andre have samme ide.

[Klik på dette link for at se kilde](https://stackoverflow.com/questions/59505849/specific-blinking-on-p5js) 

Her blev jeg bekendt med syntakserne `frameCount()` og `frameRate()`. Jeg prøvede mig frem ved bare at sætte nogle forskellige tal ind, og til sidst fik jeg den blinkende effekt jeg havde ledt efter. Det var svært for mig at forstå præcis hvad der skete, men ved lav frameRate og lav frameCount, så opstod blinke effekten. Jeg fik hjælp af vores instruktor og blev forklaret, at ved funktionen jeg har med i min kode: `if(frameCount %6 == 0)`, begynder programmet at tælle frames. Så vi ligger fokus på %6 moduloet, hvor man kan sige at programmet begynder at tælle frames ligeså snart programmet starter. Men siden vi har %6, så forsvinder/vises ordet kun når programmet når til at tal der går op i 6. Så man kan sige, at programmet kun fjerner/viser ordet når det rammer et tal på 6-tabellen. Da jeg så havde sat frameRaten til at være meget lav, så rammer programmet 6, 12 og så videre meget langsommere end hvis frameRaten f.eks. havde været 30fps. Derfor giver det den her langsomme blinke effekt.

Herunder kan den fulde kode ses.

```
if(frameCount %6 == 0) {
  frameRate(5)
  fill(0,200,255);
  noStroke()
  }
```
#### Skyerne

Skyerne er sat sammen af en masse firkanter, hvor jeg har gjort hjørnerne "bløde" og skabt den her buede effekt. Dette gjorde jeg ved at tilføje nogle ekstra værdier i funktionen.
```
square(520,70,50,70,70,70,70);
```

Jeg forsøgte at få skyerne til at flytte sig, men desværre kunne jeg ikke få det til at fungere med det tidsrum jeg havde at arbejde med. Så det må være et projekt for en anden gang. Jeg så at der var nogle af mine medstuderende der havde lykkedes med at skabe den slags animationer, så jeg håber at kunne lære af at kigge på deres koder til næste MiniX. 

## Min programmeringsoplevelse
Jeg synes det har været en lang og meget svær process, mest fordi jeg blev kastet ud i det med næsten ingen programmeringserfaring. Men det har været sjovt og jeg synes virkelig jeg forsøgte at prøve en masse forskellige ting af og er endt med et produkt, som jeg er tilfreds med. Siden programmet Visual Studio Code er overskueligt at bruge, så synes jeg hurtigt man får et godt flow og jeg fik lavet meget på forholdsvis kort tid. Så kodning virker til at blive omtalt som noget der er meget svært og nærmest farligt at kaste sig ud i, men som det også bliver nævnt i nogle af teskterne vi har læst, så burde det være muligt for alle at lære programmering. Med et program som Visual Studio Kode kan man gøre det muligt for mange at lære programmering, da der er mange referencer til det online og platformen er nem at overskue. Jeg glæder mig til at programmere noget nyt og spændende næste gang.
